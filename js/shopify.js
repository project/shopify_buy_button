var shopify_options;
var shopify_client;
var shopify_scripts_popup_html;
var shopify_products;
var shopify_money_format;
var shopify_ui;
(function ($, Drupal, drupalSettings) {
  'use strict';
  Drupal.behaviors.ShopifyModule = {
    attach: function (context, settings) {
      shopify_products = {};
      shopify_money_format = drupalSettings.shopify_buy_button.shopify_scripts_money_format;
      shopify_options = JSON.parse(drupalSettings.shopify_buy_button.shopify_scripts_product_options);
      shopify_scripts_popup_html = drupalSettings.shopify_buy_button.shopify_scripts_popup_html;
      // Creating a Shop Client
      shopify_client = ShopifyBuy.buildClient(JSON.parse(drupalSettings.shopify_buy_button.shopify_buy_button_client_options));

      // Initializing the Library
      shopify_ui = ShopifyBuy.UI.init(shopify_client);
    }
  }
})(jQuery, Drupal, drupalSettings);
