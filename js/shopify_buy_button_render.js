var ShopifyRenderModuleObject = (function ($, Drupal, drupalSettings, w) {
  'use strict';
  Drupal.behaviors.ShopifyRenderModule = {
    attach: function (context, settings) {
      // shopify-btn
      $(context).find(".shopify-btn").each(function () {

        var data_shopify_id = $(this).attr('data-shopify-id');
        var data_shopify_node = $(this).attr('data-shopify-node');

        // Add event option object into product option
        shopify_options.product.events = {
          addVariantToCart: function (product) {
            console.log('product.id:' + product.id);
            shippingModal();
          }
        };

        if (ShopifyBuy && data_shopify_id && data_shopify_node) {
          shopify_ui.createComponent('product', {
            id: data_shopify_id,
            node: document.getElementById(data_shopify_node),
            moneyFormat: shopify_money_format,
            options: shopify_options
          });
        }

      });
    }
  }
})(jQuery, Drupal, drupalSettings, window);

function shippingModal() {
  $ = jQuery;
  var modalContent = shopify_scripts_popup_html;

  var overlay = '<div class="shopify-modal">' + modalContent + "</div>";

  if ($(".shopify-modal").length == 0) {
    $("body").append(overlay);

    $("#close-shopify-modal").on("click", function () {
      $(".shopify-modal").hide();

      return false;
    });
  }
}
