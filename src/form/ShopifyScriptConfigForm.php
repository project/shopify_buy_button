<?php

namespace Drupal\shopify_buy_button\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Path\PathValidatorInterface;
use Drupal\Core\Routing\RequestContext;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides settings for the Shopify Scripts module.
 */
class ShopifyScriptConfigForm extends ConfigFormBase {

  /**
   * The path validator.
   *
   * @var \Drupal\Core\Path\PathValidatorInterface
   */
  protected $pathValidator;

  /**
   * The request context.
   *
   * @var \Drupal\Core\Routing\RequestContext
   */
  protected $requestContext;

  /**
   * Constructs an ShopifyScriptConfigForm object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\Core\Path\PathValidatorInterface $path_validator
   *   The path validator.
   * @param \Drupal\Core\Routing\RequestContext $request_context
   *   The request context.
   */
  public function __construct(ConfigFactoryInterface $config_factory, PathValidatorInterface $path_validator, RequestContext $request_context) {
    parent::__construct($config_factory);

    $this->pathValidator = $path_validator;
    $this->requestContext = $request_context;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('path.validator'),
      $container->get('router.request_context')
    );
  }

  /**
   * Getter method for Form ID.
   *
   * {@inheritdoc}
   *
   * The form ID is used in implementations of hook_form_alter() to allow other
   * modules to alter the render array built by this form controller.  it must
   * be unique site wide. It normally starts with the providing module's name.
   *
   * @return string
   *   The unique ID of the form defined by this class.
   */
  public function getFormId() {
    return 'shopify_buy_button_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'shopify_buy_button.shopify_scripts_settings',
    ];
  }

  /**
   * Build the simple form.
   *
   *   {@inheritdoc}
   *
   * A build form method constructs an array that defines how markup and
   * other form elements are included in an HTML form.
   *
   * @param array $form
   *   Default form array structure.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Object containing current form state.
   *
   * @return array
   *   The render array defining the elements of the form.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $vc = $this->config('shopify_buy_button.shopify_scripts_settings');
    $form = [];

    $form['SHOPIFY_SCRIPTS_FLDSET_INFO'] = [
      '#type' => 'html_tag',
      '#tag' => 'h3',
      '#value' => $this->t('<span style="color: red"><b>Please make validate syntax before saving.</b></span>'),
    ];
    $form['SHOPIFY_SCRIPTS_FLDSET'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Shopify Script Integration settings'),
      '#open' => TRUE,
    ];
    $form['SHOPIFY_SCRIPTS_FLDSET']['shopify_scripts_money_format'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Shopify Script Money Format'),
      '#default_value' => $vc->get('shopify_buy_button_money_format'),
      '#required' => FALSE,
      '#description' => $this->t('<span style="color: red">Please use string format.</span>'),
    ];
    $form['SHOPIFY_SCRIPTS_FLDSET']['shopify_scripts_product_options'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Shopify Script Product Options'),
      '#default_value' => $vc->get('shopify_buy_button_product_options'),
      '#required' => FALSE,
      '#description' => $this->t('<span style="color: red">Please use JSON format.</span>'),
    ];
    $form['SHOPIFY_SCRIPTS_FLDSET']['shopify_scripts_client_options'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Shopify Script Client Options'),
      '#default_value' => $vc->get('shopify_buy_button_client_options'),
      '#required' => FALSE,
      '#description' => $this->t('<span style="color: red">Please use JSON format.</span>'),
    ];
    $form['SHOPIFY_SCRIPTS_FLDSET']['shopify_scripts_popup_html'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Shopify Script Popup HTML code'),
      '#default_value' => $vc->get('shopify_buy_button_popup_html'),
      '#required' => FALSE,
      '#description' => $this->t('<span style="color: red">Enter the content for the addVariantToCart event popup in HTML format. <br>
      </span>'),
    ];
    $form['SHOPIFY_SCRIPTS_FLDSET']['enable_shopify_scripts'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable Shopify Scripts'),
      '#default_value' => $vc->get('enable_shopify_buy_button_scripts'),
      '#required' => FALSE,
    ];

    // Group submit handlers in an actions element with a key of "actions" so
    // that it gets styled correctly, and so that other modules may add actions
    // to the form. This is not required, but is convention.
    $form['actions'] = [
      '#type' => 'actions',
    ];

    // Add a submit button that handles the submission of the form.
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save Configuration'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * Implements form validation.
   *
   * The validateForm method is the default method called to validate input on
   * a form.
   *
   * @param array $form
   *   The render array of the currently built form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Object describing the current state of the form.
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
  }

  /**
   * Implements a form submit handler.
   *
   * The submitForm method is the default method called for any submit elements.
   *
   * @param array $form
   *   The render array of the currently built form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Object describing the current state of the form.
   */

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('shopify_buy_button.shopify_scripts_settings')
      ->set('shopify_buy_button_money_format', $form_state->getValue('shopify_scripts_money_format'))
      ->set('shopify_buy_button_product_options', $form_state->getValue('shopify_scripts_product_options'))
      ->set('shopify_buy_button_client_options', $form_state->getValue('shopify_scripts_client_options'))
      ->set('shopify_buy_button_popup_html', $form_state->getValue('shopify_scripts_popup_html'))
      ->set('enable_shopify_buy_button_scripts', $form_state->getValue('enable_shopify_scripts'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
