CONTENTS OF THIS FILE
---------------------
 * Introduction
 * Requirements
 * Installation
 * Configuration

INTRODUCTION
------------
Provides a flexible UI to dynamically generate shopify buy button
in to a Drupal node.

* For a full description of the module, visit the project page:
  https://www.drupal.org/project/shopify_buy_button

* To submit bug reports and feature suggestions, or track changes:
   https://www.drupal.org/project/issues/shopify_buy_button

REQUIREMENTS
------------
This module requires no modules outside of Drupal core.

INSTALLATION
------------
 * Install as you would normally install a contributed Drupal module. Visit
   https://www.drupal.org/node/1897420 for further information.

CONFIGURATION
-------------
You can configure module settings via
Configuration -> Shopify Buy Button Script Configurations
